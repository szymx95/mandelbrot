## Needed packages

 - numpy
 - pillow

## How to use

Just run mandelbrot.py and give dimensions of image.

Image "img.png" will be created

![alt text](img.png)

## TODO

    - start image in different region
    - make it faster (use multiprocessing ?)
    - better file saving
    - rendering in real time ?