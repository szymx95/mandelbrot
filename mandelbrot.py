
from PIL import Image
import numpy as np

import os
def iteratePoint(maxIter,constant):
    iter = 0
    point = complex()
    while True:
        iter += 1
        point = point * point + constant
        if abs(point) > 2:
            return iter
        if iter > maxIter:
            return iter

w = int(input("width of image:"))
h = int(input("height of image:"))
maxIter = 255
data = np.zeros((h, w, 3), dtype=np.uint8)

xMul = 2.5
xShift = 2
yMul = 2
yShift = 1

a= 0

for x in range(0,w):
    for y in range(0,h):

        point = iteratePoint(maxIter,(x/w*xMul - xShift) + (y/h*yMul - yShift)*1j )
        if point > maxIter:
            data[y][x] = [0,0,0]
        else:
            if point < 256/3:
                data[y][x] = [point,point/2,point/3]
            elif point < 256/2:
                data[y][x] = [point/2,point,point/3]
            else:
                data[y][x] = [point/2,point/3,point]
        
        a+=1
        if (a > 10000):
            os.system("cls")
            percent = "%.2f" % (  ((h*x+y) / (w*h))*100)
            print(percent + "%")
            a = 0

img = Image.fromarray(data,'RGB')
img.save('mandelbrot.png')
img.show()